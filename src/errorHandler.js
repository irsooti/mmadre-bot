const axios = require('axios');

module.exports = async err => {
  if (err.length === 0) {
    let response = await axios.post(
      'https://helper-microservices.firebaseio.com/botmother/errors.json',
      err
    );

    console.log('[ERROR]', response);
  }
};
