const rootFolder = process.cwd();
const notifierService = require("../services/notifierService");

module.exports = bot =>
  bot.hears(new RegExp("notifier"), ctx => {
    notifierService.middleware(ctx, () => {
      let text = ctx.update.message.text
        .toLowerCase()
        .split("notifier")[1]
        .trim();

      notifierService.notify(
        {
          title: "BOT MOTHER",
          message: text,
          wait: true,
          icon: rootFolder + "/src/img/me.jpg",
          sound: true,
          reply: true
        },
        function(err, data) {
          // Will also wait until notification is closed.
          console.log("Waited");
          console.log(err, data);
        }
      );

      ctx.reply("🏜 Aspetta un secondo...");
    });
  });
