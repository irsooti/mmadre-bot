const askIp = require("./askIp");
const usage = require("./usage");
const screenpage = require("./screenpage");

const hearFns = [askIp, usage, screenpage, require("./notifier")];

module.exports = {
  start: bot => {
    hearFns.forEach(hearFn => hearFn(bot));
  }
};
