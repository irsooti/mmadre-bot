const puppeteer = require("puppeteer");
const cloudinary = require("../services/cloudinaryInstance");
const rootFolder = process.cwd();

module.exports = bot =>
  bot.hears(new RegExp("screen"), ctx => {
    let text = ctx.update.message.text
      .toLowerCase()
      .split("screen")[1]
      .trim();

    if (!text.startsWith("http://")) text = `http://${text}`;

    const screenShot = (ctx, browser) => {
      cloudinary.uploader.upload(rootFolder + "/screenpage.png", result => {
        ctx.replyWithPhoto(result.secure_url);

        browser.close();
      });
    };

    (async () => {
      let browser = await puppeteer.launch();
      let page = await browser.newPage();

      page.setViewport({
        width: 1980,
        height: 1080
      });

      await page.goto(text);
      await page.screenshot({ path: "screenpage.png" });
      screenShot(ctx, browser);
    })();

    ctx.reply("🏜 Aspetta un secondo...");
  });
