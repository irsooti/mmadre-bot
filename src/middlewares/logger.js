const TelegrafLogger = require("telegraf-logger");
const axios = require("axios");

const remoteLog = async logs => {
  const req = await axios.post(
    "https://helper-microservices.firebaseio.com/botmother.json",
    logs
  );
};

const myLogger = msg => {
  let re = /\[\[(.*?)\]\]/g;
  var currentLog = {
    timestamp: new Date(),
    updateType: msg.match(re)[0].split(re)[1],
    username: msg.match(re)[1].split(re)[1],
    firstName: msg.match(re)[2].split(re)[1],
    lastName: msg.match(re)[3].split(re)[1],
    fromId: msg.match(re)[4].split(re)[1],
    subType: msg.match(re)[5].split(re)[1],
    content: msg.match(re)[6].split(re)[1]
  };
  
  remoteLog(currentLog);
};

const logger = new TelegrafLogger({
  log: myLogger, // default: console.log
  // replace or remove placeholders as necessary
  format: "[[%ut]] => [[@%u]] [[%fn]] [[%ln]] ([[%fi]]): <[[%ust]]> [[%c]]", // default
  contentLength: 100 // default
}); // All the default values can be omitted

module.exports = logger;
