const env = require('dotenv').load();
const Telegraf = require('telegraf');
const hears = require('./hears');
const logger = require('./middlewares/logger');
const startup = require('./startup');
const errorHandler = require('./errorHandler');

const bot = new Telegraf(process.env.BOT_TOKEN);

bot.use(logger.middleware());

hears.start(bot);

bot.start(ctx => ctx.reply("Apposto... ora i tirarici 'na siaggia! 💺"));

// bot.start(ctx => {
//   Telegraf.Extra.markdown().markup(m =>
//     m.inlineKeyboard([m.callbackButton("Test button", "test")])
//   );

//   bot.action("test", ctx => ctx.answerCallbackQuery("Yay!"));
// });
startup();

// bot.help(ctx => ctx.reply('Send me a sticker'));
// bot.on('sticker', ctx => ctx.reply
// bot.hears('hi', ctx => ctx.reply('Hey there'));
// bot.hears(/buy/i, ctx => ctx.reply('Buy-buy'));
bot.catch(errorHandler);

bot.startPolling();
