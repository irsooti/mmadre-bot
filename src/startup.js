const notifierService = require("./services/notifierService");

module.exports = () => {
    notifierService.start();
};
