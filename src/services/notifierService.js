const notifier = require("node-notifier");

let ctx = {};

const attachListeners = () => {
  notifier.on("timeout", function timeout() {
    ctx.reply("L'ha fatto andare in timeout...");
  });

  notifier.on("click", function clicked() {
    console.log(ctx);
    ctx.reply("Cliccato!");
  });
};

const middleware = (ctxPayload, next) => {
  ctx = ctxPayload;
  next();
};

module.exports = {
  start: attachListeners,
  middleware,
  notify: (notification) => notifier.notify(notification)
};
